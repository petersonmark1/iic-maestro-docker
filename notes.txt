readTemplate('/u01/oracle/wlserver/common/templates/wls/wls.jar')

docker run -it -p 9001:7001 -p 9002:9002 --network=InfraNET -u 0 -v c:/workspace/IIC/docker/iic-maestro-docker/OracleFMWInfrastructure/dockerfiles/12.2.1.3/properties:/u01/oracle/properties -v c:\workspace\iic\domains:/u01/oracle/user_projects/domains -e DOMAIN_NAME=soa_domain -e ADMIN_NAME=AdminServer -e ADMIN_HOST=InfraAdminContainer -e ADMIN_LISTEN_PORT=7001 -e MANAGEDSERVER_PORT=8001 -e MANAGED_NAME=soa_server1 -e ADMINISTRATION_PORT_ENABLED=true -e ADMINISTRATION_PORT=9002 -e RCUPREFIX=INFRA31 -e PRODUCTION_MODE=dev -e CONNECTION_STRING=InfraDB:1521/InfraPDB1.us.oracle.com -e USER=weblogic -e PASS=welcome1 17aee01ea6ec  bash

wlst.sh -skipWLSModuleScanning /u01/oracle/container-scripts/createInfraDomain.py -oh /u01/oracle -jh /usr/java/default -parent /u01/oracle/user_projects/domains -name soa_domain -user weblogic -password welcome1 -adminListenPort 7001 -adminName AdminServer -adminPortEnabled true -administrationPort 9002 -managedName soa_server1 -rcuDb InfraDB:1521/InfraPDB1.us.oracle.com -rcuPrefix INFRA31 -rcuSchemaPwd Oradoc_db1 -managedServerPort 8001 -prodMode dev


Installing docker in oracle linux7:

 docker run -it -u 0 -v c:\workspace\iic\docker:/u01/oracle/docker --privileged --name oraclelinuxdocker ccafb8385951  bash
 https://blogs.oracle.com/virtualization/install-docker-on-oracle-linux-7-v2
 wget http://yum.oracle.com/public-yum-ol7.repo
 cd /etc/yum.repos.d/
 vi public-yum-ol7.repo
  
 yum-config-manager --enable ol7_optional_latest
yum-config-manager --enable ol7_addons
 
  yum install docker-engine-17.06.2.ol-1.0.1.el7.x86_64.rpm
  install latest container-selinux-2.95-2.el7_6.noarch.rpm from: http://mirror.centos.org/centos/7/extras/x86_64/Packages/ 
	systemctl start docker
		Failed to get D-Bus connection: Operation not permitted
		docker run -d -it --privileged f1cebb73b3f8  /usr/sbin/init
	docker run -d -it --privileged ContainerId exec /usr/sbin/init



Domain Home is:  /u01/oracle/user_projects/domains/soa_domain
Configuring Domain for first time
Start the Admin and Managed Servers
=====================================
CONNECTION_STRING=InfraDB:1521/InfraPDB1.us.oracle.com
RCUPREFIX=INFRA31
DOMAIN_NAME=soa_domain
DOMAIN_HOME=/u01/oracle/user_projects/domains/soa_domain
Loading RCU Phase
=================
CONNECTION_STRING=InfraDB:1521/InfraPDB1.us.oracle.com
RCUPREFIX=INFRA31
jdbc_url=jdbc:oracle:thin:@InfraDB:1521/InfraPDB1.us.oracle.com
Creating Domain 1st execution
Loading SOA RCU into database
Domain Configuration Phase
==========================
 wlst.sh -skipWLSModuleScanning /u01/oracle/container-scripts/createInfraDomain.py -oh /u01/oracle -jh /usr/java/default -adminListenPort 7001 -adminName AdminServer -adminPortEnabled true -administrationPort 9002 -managedName soa_server1 -managedServerPort 8001 -prodMode dev

Initializing WebLogic Scripting Tool (WLST) ...

Welcome to WebLogic Server Administration Scripting Shell

Type help() for help on available commands

Provisioning with oracleHome:/u01/oracle javaHome:/usr/java/default domainParentDir:/u01/oracle/domain/user_projects/domains
 adminListenPort:7001 adminName:myadmin adminPortEnabled:true
 administrationPort: 9002 managedName:soa_server1 managedServerPort:8001
 prodMode:dev domainName:soa_domain domainUser:weblogic domainPassword:welcome1
 rcuDb:InfraDB:1521/InfraPDB1.us.oracle.com rcuSchemaPrefix:INFRA31 rcuSchemaPassword:Oradoc_db1
Creating Admin Server...
Creating cluster...
Creating Node Managers...
Creating Managed Server...
managed server name is soa_server1
Writing base domain...
Base domain created at /u01/oracle/domain/user_projects/domains/soa_domain
Extending domain at /u01/oracle/domain/user_projects/domains/soa_domain
Database  InfraDB:1521/InfraPDB1.us.oracle.com
Applying JRF templates...
Extension Templates added
Configuring the Service Table DataSource...
fmwDatabase  jdbc:oracle:thin:@InfraDB:1521/InfraPDB1.us.oracle.com
Getting Database Defaults...
Targeting Server Groups...
Set CoherenceClusterSystemResource to defaultCoherenceCluster for server:soa_server1
Set CoherenceClusterSystemResource to defaultCoherenceCluster for cluster:infra_cluster
Set WLS clusters as target of defaultCoherenceCluster:[infra_cluster]
Preparing to update domain...
Jun 06, 2019 9:28:38 PM oracle.mds
WARNING: MDS-11019: The default CharSet US-ASCII is not a unicode character set. File names with non-ASCII characters may not operate as expected. Check locale settings.
Domain updated successfully
provisioner.createInfraDomain completed
RetVal from Domain creation 0
/u01/oracle/container-scripts/createOrStartInfraDomain.sh: line 175: /u01/oracle/user_projects/domains/soa_domain/bin/setDomainEnv.sh: No such file or directory
Java Options: -Doracle.jdbc.fanEnabled=false -Dweblogic.StdoutDebugEnabled=false
Starting the Admin Server
==========================
/u01/oracle/container-scripts/createOrStartInfraDomain.sh: line 186: /u01/oracle/user_projects/domains/soa_domain/startWebLogic.sh: No such file or directory
touch: cannot touch '/u01/oracle/user_projects/domains/soa_domain/servers/AdminServer/logs/AdminServer.log': No such file or directory
tail: cannot open '/u01/oracle/user_projects/domains/soa_domain/servers/AdminServer/logs/AdminServer.log' for reading: No such file or directory
tail: no files remaining
PS C:\workspace\IIC\docker\iic-maestro-docker\OracleFMWInfrastructure\dockerfiles\12.2.1.3> docker images
REPOSITORY                                          TAG                 IMAGE ID            CREATED             SIZE
oracle/fmw-infrastructure                           12.2.1.3            d93f00a3ba08        8 minutes ago       2.43GB
<none>                                              <none>              f19cc4771c20        9 minutes ago       5.67GB
oracle/serverjre                                    8                   a3773bea48e9        28 minutes ago      387MB
oraclelinux                                         7.6                 ccafb8385951        6 days ago          235MB
container-registry.oracle.com/database/enterprise   12.2.0.1            12a359cd0528        21 months ago       3.44GB
PS C:\workspace\IIC\docker\iic-maestro-docker\OracleFMWInfrastructure\dockerfiles\12.2.1.3> docker run -it -p 9001:7001 -p 9002:9002 --network=InfraNET -u 0 -v c:/workspace/IIC/docker/iic-maestro-docker/OracleFMWInfrastructure/dockerfiles/12.2.1.3/properties:/u01/oracle/properties -v c:\workspace\iic\domain:/u01/oracle/user_projects/domains -e DOMAIN_NAME=soa_domain -e ADMIN_NAME=AdminServer -e ADMIN_HOST=InfraAdminContainer -e ADMIN_LISTEN_PORT=7001 -e MANAGEDSERVER_PORT=8001 -e MANAGED_NAME=soa_server1 -e ADMINISTRATION_PORT_ENABLED=true -e ADMINISTRATION_PORT=9002 -e RCUPREFIX=INFRA31 -e PRODUCTION_MODE=dev -e CONNECTION_STRING=InfraDB:1521/InfraPDB1.us.oracle.com d93f00a3ba08  bash

[root@1ed5b16030e4 oracle]#
[root@1ed5b16030e4 oracle]# cd /u01/oracle
[root@1ed5b16030e4 oracle]# pwd
/u01/oracle
[root@1ed5b16030e4 oracle]# ls
OPatch       coherence          em         oraInst.loc    oui         root.sh        wlserver
cfgtoollogs  container-scripts  inventory  oracle_common  properties  user_projects
[root@1ed5b16030e4 oracle]# cd user_projects/
[root@1ed5b16030e4 user_projects]# cd domains/
[root@1ed5b16030e4 domains]# cd soa_domain/
[root@1ed5b16030e4 soa_domain]# cd servers/
[root@1ed5b16030e4 servers]# cd AdminServer/
[root@1ed5b16030e4 AdminServer]# dir
security
[root@1ed5b16030e4 AdminServer]# cd ..
[root@1ed5b16030e4 servers]# dir
AdminServer
[root@1ed5b16030e4 servers]# cd ..
[root@1ed5b16030e4 soa_domain]# ls
servers
[root@1ed5b16030e4 soa_domain]# cd servers/
[root@1ed5b16030e4 servers]# dir
AdminServer
[root@1ed5b16030e4 servers]# cd ..
[root@1ed5b16030e4 soa_domain]# ls
servers
[root@1ed5b16030e4 soa_domain]# dir
servers
[root@1ed5b16030e4 soa_domain]# cd ..
[root@1ed5b16030e4 domains]# ls
base_domain  soa_domain
[root@1ed5b16030e4 domains]# cd base_domain/
[root@1ed5b16030e4 base_domain]# dir
servers
[root@1ed5b16030e4 base_domain]# cd servers/
[root@1ed5b16030e4 servers]# dir
myadmin
[root@1ed5b16030e4 servers]# cd myadmin/
[root@1ed5b16030e4 myadmin]# dir
security
[root@1ed5b16030e4 myadmin]# cd security/
[root@1ed5b16030e4 security]# dir
boot.properties
[root@1ed5b16030e4 security]# cd ..
[root@1ed5b16030e4 myadmin]# cd ..
[root@1ed5b16030e4 servers]# cd ..
[root@1ed5b16030e4 base_domain]# cd ..
[root@1ed5b16030e4 domains]# pwd
/u01/oracle/user_projects/domains
[root@1ed5b16030e4 domains]#
[root@1ed5b16030e4 domains]#









































































































