# Change location to domain custom tree
myProperties = 'listenAddress.properties'
loadProperties(myProperties)
connect('weblogic','oracle1234','t3://' + listenAddress + ':7001')
domainCustom()

# Setting variables
myMBean=ObjectName('emoms.props:Location=AdminServer,name=emoms.properties,type=Properties,Application=em')
types=['java.lang.String', 'java.lang.String']
type=['java.lang.String']

# Set property oracle.sysman.emas.discovery.wls.FMW_DISCOVERY_USE_CACHED_RESULTS=true
params=['oracle.sysman.emas.discovery.wls.FMW_DISCOVERY_USE_CACHED_RESULTS', 'true']
mbs.invoke(myMBean,'setProperty',params,types)

# Set property oracle.sysman.emas.discovery.wls.FMW_DISCOVERY_MAX_CACHE_AGE=7200000
params=['oracle.sysman.emas.discovery.wls.FMW_DISCOVERY_MAX_CACHE_AGE','7200000']
mbs.invoke(myMBean,'setProperty',params,types)

# Set property oracle.sysman.emas.discovery.wls.FMW_DISCOVERY_MAX_WAIT_TIME=10000
params=['oracle.sysman.emas.discovery.wls.FMW_DISCOVERY_MAX_WAIT_TIME','10000']
mbs.invoke(myMBean,'setProperty',params,types)

