#================================
This Dockerfile extends the Oracle SOA Suite with Maestro configurations, DB schemas and bpm proxy

# How to build and run

Refer to the following wiki on how to use:
https://avioconsulting.atlassian.net/wiki/spaces/IIC/pages/544571527/Maestro+and+SOA+Suite+12c+Docker+Image+Guide+For+Developers

