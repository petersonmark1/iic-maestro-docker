1. Get an EC2 xlarge instance to build the image. 50GB of storage should be fine. AMI linux is fine

2. Docker
  sudo yum install -y docker
  # base size is not big enough
  sudo sed -i 's/DOCKER_STORAGE_OPTIONS=/DOCKER_STORAGE_OPTIONS="--storage-opt dm.basesize=20GB"/' /etc/sysconfig/docker-storage
  sudo service docker start
