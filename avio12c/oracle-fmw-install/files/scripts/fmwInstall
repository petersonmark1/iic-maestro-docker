#!/bin/bash -e

unset DISPLAY

usage() {
    echo "Usage: $0 ENV"
    exit 1
}

[[ $# -ne 1 ]] && usage


DIR=`dirname $0`
SCRIPT_DIR=`cd $DIR && pwd -P`
CONFIG_DIR=$SCRIPT_DIR/config
FMW_ENV=$1


if [ ! -f $CONFIG_DIR/${FMW_ENV}_install_config.properties ]; then
    echo "Environment configuration [$CONFIG_DIR/${FMW_ENV}_install_config.properties] not found"
    usage
fi

echo "Loading properties from [$CONFIG_DIR/${FMW_ENV}_install_config.properties]"
source $CONFIG_DIR/${FMW_ENV}_install_config.properties


if [ -f $JAVA_HOME/bin/sparcv9/java ]; then
	JAVA=$JAVA_HOME/bin/sparcv9/java
else
	JAVA=$JAVA_HOME/bin/java
fi

if [ -d $FMW_TMP_DIR ]; then
	rm -rf $FMW_TMP_DIR
fi

echo "Creating tmp directory [$FMW_TMP_DIR]"
mkdir -p $FMW_TMP_DIR
chmod 777 $FMW_TMP_DIR

createOraInstLoc() {
	if [ -f "$ORA_INST_LOC" ]
	then
        	echo "Removing existing oraInst.loc at $ORA_INST_LOC"
        	rm $ORA_INST_LOC
	fi

	echo "inventory_loc=$ORACLE_INVENTORY" > $ORA_INST_LOC
	echo "inst_group=$ORACLE_GROUP" >> $ORA_INST_LOC
}

installFmwInfra() {
	RSP_FILE="$FMW_TMP_DIR/fmw_infrastructure.response"
	echo "Installing FMW Infrastructure..."
	echo "[ENGINE]" > $RSP_FILE
	echo "Response File Version=1.0.0.0.0" >> $RSP_FILE
	echo "[GENERIC]" >> $RSP_FILE
	echo "ORACLE_HOME=$ORACLE_HOME" >> $RSP_FILE
	echo "INSTALL_TYPE=Fusion Middleware Infrastructure" >> $RSP_FILE
	echo "MYORACLESUPPORT_USERNAME=" >> $RSP_FILE
	echo "MYORACLESUPPORT_PASSWORD=<SECURE VALUE>" >> $RSP_FILE
	echo "DECLINE_SECURITY_UPDATES=true" >> $RSP_FILE
	echo "SECURITY_UPDATES_VIA_MYORACLESUPPORT=false" >> $RSP_FILE
	echo "PROXY_HOST=" >> $RSP_FILE
	echo "PROXY_PORT=" >> $RSP_FILE
	echo "PROXY_USER=" >> $RSP_FILE
	echo "PROXY_PWD=<SECURE VALUE>" >> $RSP_FILE
	echo "COLLECTOR_SUPPORTHUB_URL=" >> $RSP_FILE 
	$JAVA -jar $FMW_BINARIES/fmw_12.2.1.3.0_infrastructure.jar -silent -responseFile $RSP_FILE -invPtrLoc $ORA_INST_LOC
}

installSOA() {
	RSP_FILE="$FMW_TMP_DIR/fmw_soa.response"
	echo "Installing SOA Suite..."
        echo "[ENGINE]" > $RSP_FILE
        echo "Response File Version=1.0.0.0.0" >> $RSP_FILE
        echo "[GENERIC]" >> $RSP_FILE
        echo "ORACLE_HOME=$ORACLE_HOME" >> $RSP_FILE
        echo "INSTALL_TYPE=BPM" >> $RSP_FILE
	$JAVA -jar $FMW_BINARIES/fmw_12.2.1.3.0_soa.jar -silent -responseFile $RSP_FILE -invPtrLoc $ORA_INST_LOC
}

installOSB() {
	RSP_FILE="$FMW_TMP_DIR/fmw_osb.response"
	echo "Installing OSB..."
        echo "[ENGINE]" > $RSP_FILE
        echo "Response File Version=1.0.0.0.0" >> $RSP_FILE
        echo "[GENERIC]" >> $RSP_FILE
        echo "ORACLE_HOME=$ORACLE_HOME" >> $RSP_FILE
        echo "INSTALL_TYPE=Service Bus" >> $RSP_FILE
	$JAVA -jar $FMW_BINARIES/fmw_12.2.1.3.0_osb.jar -silent -responseFile $RSP_FILE -invPtrLoc $ORA_INST_LOC
}

applyPatches() {
	PATCH_LIST=$(IFS=, ; echo "${FMW_PATCHES[*]}")
	echo "Applying patches [$PATCH_LIST] from [$FMW_PATCH_DIR]"

	$ORACLE_HOME/OPatch/opatch napply $FMW_PATCH_DIR -id $PATCH_LIST -silent -invPtrLoc $ORA_INST_LOC
}

createOraInstLoc
installFmwInfra
installSOA
installOSB

rm -rf $FMW_TMP_DIR
