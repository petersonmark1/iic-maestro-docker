########################################################################
# Common Paths
######################################################################## 
DOMAIN      = 'bpm_domain'
JAVA_HOME   = '/opt/oracle/product/java/jdk1.8'
ORACLE_HOME = '/opt/oracle/product/oracle_home'
DOMAIN_BASE = '/opt/oracle/config/domains'
APP_BASE     = '/opt/oracle/config/applications' 
LOG_BASE    = '/opt/oracle/logs'
RUNTIME_SCRIPT_CONFIG = '/opt/oracle/scripts/config'
DOMAIN_HOME =  DOMAIN_BASE + '/' + DOMAIN
# For use with split domain directories
# DOMAIN_HOME =  DOMAIN_BASE + '/' + DOMAIN + '/aserver/' + DOMAIN
# MSERVER_DOMAIN_HOME =  DOMAIN_BASE + '/' + DOMAIN + '/mserver/' + DOMAIN
APP_HOME = APP_BASE + '/' + DOMAIN

WL_HOME = ORACLE_HOME + '/wlserver'
BASE_TEMPLATE = WL_HOME + '/common/templates/wls/wls.jar'

########################################################################
# Database Configuration
######################################################################## 
DB_HOST = 'soa-db'
DB_PORT = '1521'
DB_SERVICE = 'SOADB'
DB_URL = 'jdbc:oracle:thin:@//' + DB_HOST + ':' + DB_PORT + '/' + DB_SERVICE
DB_PREFIX = 'DEV'
DB_PASSWORD = 'oracle1234'

DB_SCHEMAS=['MDS','IAU','IAU_APPEND','IAU_VIEWER','OPSS','UCSUMS', 'WLS' ,'STB', 'SOAINFRA']
DB_SYS_USER='sys'
DB_SYS_PASSWORD='oracle'

DB_GRIDLINK = True

########################################################################
# Common Configurations
########################################################################
JSSE_ENABLED = True
DEVELOPMENT_MODE = True

LOG_DIR = LOG_BASE + '/' + DOMAIN
LOG_COUNT = 10
LOG_SIZE = 10000  

JTA_TIMEOUT = 600

########################################################################
# AdminServer Configuration
######################################################################## 
ADMIN_SERVER   = 'AdminServer'
ADMIN_USER     = 'weblogic'
ADMIN_PASSWORD = 'oracle1234'
ADMIN_MACHINE = 'ADMIN_HOST'
ADMIN_MACHINE_HOST = 'localhost'
ADMIN_MACHINE_PORT = 5556
ADMIN_MACHINE_TYPE='Plain'
ADMIN_HOST = ''
ADMIN_PORT = 7001
ADMIN_SERVER_GROUPS = ['WSMPM-MAN-SVR', 'JRF-MAN-SVR', 'WSM-CACHE-SVR']
# If using standalone WSM Cluster
# ADMIN_SERVER_GROUPS = ['JRF-MAN-SVR']
ADMIN_START_ARGS = '-XX:PermSize=128m -XX:MaxPermSize=512m -Xms512m -Xmx2G -Dsvnkit.useJNA=false'

########################################################################
# SOA/OSB/BAM Configuration
########################################################################

# The following configuration for number of nodes and machines are
# used for WSM, SOA, OSB and BAM.  These 4 clusters are scaled equally on
# all machines.

ENABLE_SOA=True
ENABLE_BPM=True
ENABLE_OSB=True
ENABLE_BAM=True
ENABLE_WSM=False
# To enable standalone WSM Cluster
# ENABLE_WSM=True

MANAGED_NODES = 1
MANAGED_NM_HOSTS = ['localhost','localhost']
MANAGED_NM_PORTS = [5556, 5556]
MANAGED_NM_TYPE = 'Plain'

CLUSTER_DIR = '/opt/oracle/config/cluster'

SOA_HOSTS = ['','']
OSB_HOSTS = ['','']
BAM_HOSTS = ['','']
WSM_HOSTS = ['','']

SOA_PORTS = [8001,8002]
OSB_PORTS = [9001,9002]
BAM_PORTS = [10001,10002]
WSM_PORTS = [11001,11002]

SOA_START_ARGS = '-XX:PermSize=128m -XX:MaxPermSize=1G -Xms512m -Xmx2G -Dsvnkit.useJNA=false'
OSB_START_ARGS = '-XX:PermSize=128m -XX:MaxPermSize=512m -Xms512m -Xmx2G'
BAM_START_ARGS = '-XX:PermSize=128m -XX:MaxPermSize=512m -Xms512m -Xmx2G'
WSM_START_ARGS = '-XX:PermSize=128m -XX:MaxPermSize=512m -Xms512m -Xmx2G'
 
SOA_SERVER_BASE_NAME = 'soa_server'
SOA_CLUSTER_NAME = 'soa_cluster'
SOA_HTTP_PLUGIN_ENABLED = True
SOA_FRONTEND_HOST = ''
SOA_FRONTEND_HTTP_PORT = 8001
SOA_FRONTEND_HTTPS_PORT = 8002

OSB_SERVER_BASE_NAME = 'osb_server'
OSB_CLUSTER_NAME = 'osb_cluster'
OSB_HTTP_PLUGIN_ENABLED = True
OSB_FRONTEND_HOST = ''
OSB_FRONTEND_HTTP_PORT = 0
OSB_FRONTEND_HTTPS_PORT = 0

BAM_SERVER_BASE_NAME = 'bam_server'
BAM_CLUSTER_NAME = 'bam_cluster'
BAM_HTTP_PLUGIN_ENABLED = True
BAM_FRONTEND_HOST = ''
BAM_FRONTEND_HTTP_PORT = 0
BAM_FRONTEND_HTTPS_PORT = 0

WSM_SERVER_BASE_NAME = 'wsm_server'
WSM_CLUSTER_NAME = 'wsm_cluster'
WSM_HTTP_PLUGIN_ENABLED = True
WSM_FRONTEND_HOST = ''
WSM_FRONTEND_HTTP_PORT = 0
WSM_FRONTEND_HTTPS_PORT = 0

WSM_SERVER_GROUPS = ['WSMPM-MAN-SVR', 'JRF-MAN-SVR', 'WSM-CACHE-SVR']
if(ENABLE_WSM):
    SOA_SERVER_GROUPS=['SOA-MGD-SVRS-ONLY']
    OSB_SERVER_GROUPS=['OSB-MGD-SVRS-ONLY']
    BAM_SERVER_GROUPS=['BAM12-MGD-SVRS-ONLY']
else:
    SOA_SERVER_GROUPS=['SOA-MGD-SVRS']
    OSB_SERVER_GROUPS=['OSB-MGD-SVRS-COMBINED']
    BAM_SERVER_GROUPS=['BAM12-MGD-SVRS']

SOA_TLOG_DIR = CLUSTER_DIR + '/' + SOA_CLUSTER_NAME
BAM_TLOG_DIR = CLUSTER_DIR + '/' + BAM_CLUSTER_NAME
WSM_TLOG_DIR = CLUSTER_DIR + '/' + WSM_CLUSTER_NAME
OSB_TLOG_DIR = CLUSTER_DIR + '/' + OSB_CLUSTER_NAME

