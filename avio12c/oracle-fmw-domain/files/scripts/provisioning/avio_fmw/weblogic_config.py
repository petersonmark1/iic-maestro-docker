'''
Created on Jul 20, 2015

@author: adesjardin
'''

from java.io import FileInputStream
from java.util import Properties

class WeblogicConfig(object):

    def __init__(self, adminHost = 'fmwvm', adminPort = 7001, adminUsername = 'weblogic', adminPassword = 'avio1234', domain = 'bpm_domain', soaCluster = 'soa_cluster'):
        self.adminHost = adminHost
        self.adminPort = adminPort
        self.adminUsername = adminUsername
        self.adminPassword = adminPassword
        self.domain = domain
        self.soaCluster = soaCluster
        self.properties = Properties()
        
    def loadFromProperties(self, propertyFile):
        self.properties.load(FileInputStream(propertyFile)) 
        wlTaskUrl = str(self.properties.getProperty("wltasks.url"))
        wlTaskUrl = wlTaskUrl.replace('t3://', '')
        self.adminHost, self.adminPort = wlTaskUrl.split(':')
        self.adminUsername = str(self.properties.getProperty("wlusername"))
        self.adminPassword = str(self.properties.getProperty("wlpassword"))
        self.soaCluster = str(self.properties.getProperty("soa.cluster"))
        self.spacesCluster = str(self.properties.getProperty("spaces.cluster"))
        self.fileStorePath = str(self.properties.getProperty("filestore.path"))
        
    def getAdminUrl(self):
        return "t3://" + self.adminHost + ":" + str(self.adminPort)
        
    def getProperty(self, propertyName):
        return str(self.properties.getProperty(propertyName))