'''
Created on Jul 20, 2015

@author: adesjard
'''

from java.util import Properties
from java.io import File
import sys

class WeblogicOfflineConfigurator(object):
    def __init__(self, config):
        self.config = config
        self.wlst = __import__('wlst.wlstModule').wlstModule
        
    def readDomain(self, domainHome, applicationHome):
        self.wlst.readDomain(domainHome)
	self.wlst.setOption('AppDir', applicationHome)
        
    def writeDomain(self, domainHome):
        print '------------------------------------------------------'
        print 'Writing domain to [' + domainHome + ']'
        self.wlst.writeDomain(domainHome)
        self.wlst.closeTemplate()
        print 'Domain written successfully'
        print '------------------------------------------------------'        
        
    def updateDomain(self):
        self.logOperationStart('Updating domain')
        try:
            self.wlst.updateDomain()
            self.wlst.dumpStack()
            self.wlst.closeDomain()
        except: 
            e = sys.exc_info()[0]
            print( "Error: %s" % e )
            self.wlst.dumpStack()

            
    def createDomain(self, baseTemplate, javaHome, applicationHome, developmentMode, adminUser, adminPassword):
        """
        Performs the following base domain creation tasks:
        - Create a domain using the BASE_TEMPLATE
        - Set the Application Directory
        - Set Domain Java Home
        - Set Development or Production Mode
        - Set the admin(weblogic) user name and password
        """
        print('Loading template: ' + baseTemplate)
        self.wlst.readTemplate(baseTemplate)
        
        print('Setting application directory: ' + applicationHome)
        self.wlst.setOption( "AppDir", applicationHome)
        
        print('Setting JAVA_HOME: ' + javaHome)
        self.wlst.setOption('JavaHome', javaHome)
        
        if developmentMode == True:
            print('Development mode enabled')
            self.wlst.setOption('ServerStartMode', 'dev')
        else:
            print('Production mode enabled')
            self.wlst.setOption('ServerStartMode', 'prod')
        
        print('Setting admin user and password')
        self.wlst.cd('/Security/base_domain/User/weblogic')     
        self.wlst.set('Name', adminUser)
        self.wlst.set('Password', adminPassword)         
        
    def configureDomainLogs(self, domain, logCount, logSize, logDir):
        """
        Configures the log settings for the domain based on the common log settings.
        """        
        print '------------------------------------------------------'
        print 'Configuring domain log settings'
        print '------------------------------------------------------'
        self.wlst.cd('/')
        self.wlst.create(domain,'Log')
        self.setAttributes('/Log/' + domain, 
                           {
                            'FileCount' : logCount, 
                            'FileMinSize' : logSize, 
                            'FileName' : logDir + '/' + domain + '/' + domain + '.log'
                            })
        
    def createMachine(self, machine, listenAddress, port, listenerType):
        """
        Creates a UNIX machine with the provided name, listen address, and port.  
        The listener type is set to either ssl or plain.
        """            
        print '------------------------------------------------------'
        print 'Creating machine [' + machine + ']'
        print '------------------------------------------------------'
        self.wlst.cd('/')
        self.wlst.create(machine,'UnixMachine')
        self.wlst.cd('/UnixMachine/' + machine)
        self.wlst.create(machine, 'NodeManager')
        self.wlst.cd('NodeManager/' + machine)
        self.setAttributes('/UnixMachine/' + machine + '/NodeManager/' + machine, 
                           {
                            'ListenAddress' : listenAddress,
                            'ListenPort' : port,
                            'NMType' : listenerType
                            })
        
    def configureServer(self, server, machine, listenAddress, port, startArgs, javaHome, jsseEnabled):
        """
        Configures the following for the provided server:
        - Assigned machine
        - Listen address and port
        - Server start arguments (Heap Size, Perm Gen, etc...)
        - Disables SSL and Hostname Verification
        - Optionally enables JSSE
        """    
        print '------------------------------------------------------'
        print 'Configuring basic settings for server [' + server + ']'
        print '------------------------------------------------------'
        self.wlst.cd('/Servers/' + server)
        self.wlst.create(server,'ServerStart')
        self.wlst.create(server, 'SSL')
        
        self.setAttributes('/Servers/' + server, {
                                                  'Machine' : machine,
                                                  'ListenAddress' : listenAddress,
                                                  'ListenPort' : port
                                                  })
        self.setAttributes('/Servers/' + server + '/ServerStart/' + server, {
                                                                             'Arguments' : startArgs,
                                                                             'JavaVendor' : 'Sun',
                                                                             'JavaHome' : javaHome
                                                                             })
        self.setAttributes('/Servers/' + server + '/SSL/' + server, {
                                                                     'Enabled' : 'False',
                                                                     'HostNameVerificationIgnored' : 'True',
                                                                     'JSSEEnabled' : str(jsseEnabled)
                                                                     })
    def configureTLogs(self, server, tlogDir):
        print '------------------------------------------------------'
        print 'Configuring Transaction Logs for server [' + server + ']'
        print '------------------------------------------------------'
	self.wlst.cd('/Servers/' + server)
	self.wlst.create(server, 'DefaultFileStore')
        self.setAttributes('/Servers/' + server + '/DefaultFileStore/' + server, { 'Directory' : tlogDir })        
       
        
    def configureServerLogs(self, server, logCount, logSize, logDir):
        """
        Configures the log settings for the provided server based on the common log settings.
        """    
        print '------------------------------------------------------'
        print 'Configuring log settings for server [' + server + ']'
        print '------------------------------------------------------'
        self.wlst.cd('/Servers/' + server)
        self.wlst.create(server,'Log')
        self.setAttributes('/Server/' + server + '/Log/' + server, {
                                                                    'FileCount' : logCount,
                                                                    'FileMinSize' : logSize,
                                                                    'FileName' : logDir + '/' + server + '/' + server + '.log'
                                                                    })  
    def setDomainCredential(self, domain, adminPassword, domainHome):
        """
        Configures the domain credential
        """
        print '------------------------------------------------------'
        print 'Setting domain credential to [' + adminPassword + ']'
        print '------------------------------------------------------'        
        encryptedCredential = self.wlst.encrypt(adminPassword, domainHome)    
        self.setAttributes('/SecurityConfiguration/' + domain, {'CredentialEncrypted' : encryptedCredential})
        
    def setNmCredential(self, domain, domainHome, nmUser, nmPassword):
        """
        Configures the node manager credential used for communication between
        the AdminServer and the nodemanager.
        """
        self.logOperationStart('Setting node manager credential to [' + nmPassword + ']')
        encryptedCredential = self.encryptCredential(domainHome, nmPassword)
        self.setAttributes('/SecurityConfiguration/'+ domain, {
                                                               'NodeManagerUsername' : nmUser,
                                                               'NodeManagerPasswordEncrypted' : encryptedCredential
                                                               })
    def createBootProps(self, domainHome, serverName, adminUser, adminPassword):
        """
        Creates a boot.properties file for the provided server
        """        
        self.logOperationStart('Creating boot.properties for server [' + serverName + ']')
        securityPath = domainHome + '/servers/' + serverName + '/security'
        props = Properties()
        props.setProperty('username', adminUser)
        props.setProperty('password', adminPassword)
        self.writeProperties(props, securityPath, 'boot.properties')

    def createStartupProps(self, domainHome, serverName, startArgs):
        """
        Creates a startup.properties file for the provided server
        """    
        self.logOperationStart('Creating startup.properties for server [' + serverName + ']')
        nmPath = domainHome + '/servers/' + serverName + '/data/nodemanager'
        props = Properties()
        props.setProperty('Arguments', startArgs)
        self.writeProperties(props, nmPath, 'startup.properties')      
        
    def createNmPasswordProps(self, domainHome, nmUser, nmPassword):
        """
        Creates a nm_password.properties for the nodemanager
        """    
        self.logOperationStart('Creating nm_password.properties')
        nmPath = domainHome + '/config/nodemanager'    
        props = Properties()
        props.setProperty('username', nmUser)
        props.setProperty('password', nmPassword)
        self.writeProperties(props, nmPath, 'nm_password.properties')     
             
    def configureNodeManager(self, domainHome, nmHost, nmPort, nmListnerType):
        """
        Configures the following properties for the nodemanager:
        - Listen port
        - Listener type: plain or ssl
        """        
        self.logOperationStart('Configuring node manager settings')
        nmPath = domainHome + '/nodemanager'
        props = self.readProperties(nmPath, 'nodemanager.properties')
        props.setProperty('ListenPort', str(nmPort))
        props.setProperty('ListenAddress', nmHost)
        if nmListnerType == 'Plain':
            props.setProperty('SecureListener', 'false')
        else:
            props.setProperty('SecureListener', 'true')
        self.writeProperties(props, nmPath, 'nodemanager.properties')             

    def createUserOverrides(self, domainHome):
        self.logOperationStart('Creating setUserOverrides.sh script')
        f = open(domainHome + '/bin/setUserOverrides.sh', 'w')
        f.write('#!/bin/sh\n')
        f.write('\n')
        f.write('unset DISPLAY\n')
        f.close()        
        
    def addTemplate(self, template):
            self.logOperationStart('Adding template to domain [' + template + ']')
            self.wlst.addTemplate(template)
    
    def configureDataSource(self, datasource, dbUrl, dbUser, dbPassword):
        self.logOperationStart('Updating connection details for [' + datasource + ']') 
        self.setAttributes(
                           '/JDBCSystemResource/' + datasource + '/JdbcResource/' + datasource + '/JDBCDriverParams/NO_NAME_0', 
                           {
                            'URL' : dbUrl,
                            'PasswordEncrypted' : dbPassword,
                            'DriverName' : JdbcDatasource.ORACLE_DRIVER
                            })       
        self.setAttributes(
                           '/JDBCSystemResource/LocalSvcTblDataSource/JdbcResource/LocalSvcTblDataSource/JDBCDriverParams/NO_NAME_0/Properties/NO_NAME_0/Property/user', 
                           {
                            'Value' : dbUser
                            })        
    
    def configureServiceTableConnection(self, dbUrl, dbPrefix, dbPassword):
        """
        Reconfigures the LocalScvTblDataSource data source using the provided DB connection information
        """
        self.logOperationStart('Updating connection details for LocalScvTblDataSource')
        self.setAttributes(
                           '/JDBCSystemResource/LocalSvcTblDataSource/JdbcResource/LocalSvcTblDataSource/JDBCDriverParams/NO_NAME_0', 
                           {
                            'URL' : dbUrl,
                            'PasswordEncrypted' : dbPassword,
                            'DriverName' : JdbcDatasource.ORACLE_DRIVER
                            })       
        self.setAttributes(
                           '/JDBCSystemResource/LocalSvcTblDataSource/JdbcResource/LocalSvcTblDataSource/JDBCDriverParams/NO_NAME_0/Properties/NO_NAME_0/Property/user', 
                           {
                            'Value' : dbPrefix + '_STB'
                            })
        
    def getDatabaseDefaults(self):
        self.logOperationStart('Retrieving database defaults from LocalScvTblDataSource')
        self.wlst.getDatabaseDefaults()
        self.wlst.dumpStack()
        
    def configureDataSourceForXA(self, datasourceName):
        self.logOperationStart('Configuring datasource for XA connections [' + datasourceName + ']')
        self.setAttributes('/JDBCSystemResource/' + datasourceName + '/JdbcResource/' + datasourceName + '/JDBCDriverParams/NO_NAME_0', 
                           {
                            'DriverName' : JdbcDatasource.ORACLE_XA_DRIVER,
                            'UseXADataSourceInterface' : 'True'
                            })    
        self.setAttributes('/JDBCSystemResource/' + datasourceName + '/JdbcResource/' + datasourceName + '/JDBCDataSourceParams/NO_NAME_0', 
                           {
                            'GlobalTransactionsProtocol' : TransactionType.TWO_PHASE_COMMIT
                            })

    def configureDataSourceForGridlink(self, datasourceName):
        self.logOperationStart('Configuring datasource for Gridlink [' + datasourceName + ']')

        self.wlst.cd('/JDBCSystemResource/' + datasourceName + '/JdbcResource/' + datasourceName)
        self.wlst.create('oracleParams', 'JDBCOracleParams')
        self.wlst.cd('JDBCOracleParams/NO_NAME_0')
        self.setAttributes('/JDBCSystemResource/' + datasourceName + '/JdbcResource/' + datasourceName + '/JDBCOracleParams/NO_NAME_0',
                           {
                            'FanEnabled' : 'False',
                            'ActiveGridlink' : 'True'
                            })   
        
    def createCluster(self, name, messagingMode):
        self.logOperationStart('Creating cluster [' + name +  '] with messaging mode [' + messagingMode + ']')
        self.wlst.cd('/')
        cluster = self.wlst.create(name, 'Cluster')
        cluster.setClusterMessagingMode(messagingMode)

    def configureClusterHttp(self, cluster, pluginEnabled, frontendHost, frontendHttpPort, frontendHttpsPort):
        self.setAttributes('/Clusters/' + cluster, 
                           {
                            'FrontendHost' : frontendHost,
                            'FrontendHTTPPort' : frontendHttpPort,
                            'FrontendHTTPSPort' : frontendHttpsPort,
                            'WeblogicPluginEnabled' : pluginEnabled
                            })  
        
    def renameServer(self, serverName, newName):
        self.logOperationStart('Renaming server [' + serverName + '] to [' + newName +']')
        self.setAttributes('/Server/' + serverName, {'Name' : newName})
        
    def createServer(self, name):
        self.logOperationStart('Creating server [' + name + ']')
        self.wlst.cd('/')
        self.wlst.create(name, 'Server')
        
    def addServerToCluster(self, server, cluster):
        self.logOperationStart('Assigning server [' + server + '] to cluser [' + cluster + ']')
        self.wlst.assign('Server', server, 'Cluster', cluster)
    
    def setServerGroups(self, server, serverGroups):
        self.logOperationStart('Assigning server groups ' + str(serverGroups) + ' to server [' + server + ']')
        self.wlst.setServerGroups(server, serverGroups)

    def setApplicationTargets(self, application, targets):
        self.wlst.cd('/AppDeployments/' + application)
        self.wlst.set('Target', targets)
    
    def readProperties(self, path, filename):
        """
        Read the properties from the provided path and filename
        """
        props = Properties()
        propFile = open(path + '/' + filename, 'r')
        props.load(propFile)
        propFile.close()
        return props
            
    def writeProperties(self, props, path, filename):
        """
        Write the provided properties to a file in the provided directory
        """
        File(path).mkdirs()
        propFile = open(path + '/' + filename, 'w')
        props.store(propFile, 'Created by domain creation script')
        propFile.flush()
        propFile.close()             

    def setAttributes(self, mbean, attributes):
        print 'Updating MBean [' + mbean + ']'
        self.wlst.cd(mbean)
        for a in attributes:
            print '--- Setting [' + a + '=' + str(attributes[a]) + ']'
            self.wlst.set(a, attributes[a])   
            
    def printAttributes(self, mbean, attributes):
        print 'MBean [' + mbean + ']'
        self.wlst.cd(mbean)
        for a in attributes:
            print '--- [' + a + '=' + str(self.wlst.get(a)) + ']'    
            
    def encryptCredential(self, domainHome, credential):
        return self.wlst.encrypt(credential, domainHome)
    
    def logOperationStart(self, message):
        print '\n------------------------------------------------------'
        print message
        print '------------------------------------------------------\n'      
        
class WeblogicOnlineConfigurator(object):
    
    SOA_CONFIG_MBEAN='oracle.as.soainfra.config:Location=SERVER_NAME,name=soa-infra,type=SoaInfraConfig,Application=soa-infra'
    EM_FMW_CTRL_MBEAN='emoms.props:Location=AdminServer,name=emoms.properties,type=Properties,Application=em'
    FMW_DISCOVERY_USE_CACHED_RESULTS='oracle.sysman.emas.discovery.wls.FMW_DISCOVERY_USE_CACHED_RESULTS'
    FMW_DISCOVERY_MAX_CACHE_AGE='oracle.sysman.emas.discovery.wls.FMW_DISCOVERY_MAX_CACHE_AGE'
    FMW_DISCOVERY_MAX_WAIT_TIME='oracle.sysman.emas.discovery.wls.FMW_DISCOVERY_MAX_WAIT_TIME'
    
    
    def __init__(self, config):
        self.config = config
        self.jms = JmsConfigurator(self)
        self.wlst = __import__('wlstModule')
        
    def connect(self):
        print "Connecting to " + self.config.domain + " at " + self.config.getAdminUrl()
        wls.connect(self.config.adminUsername, self.config.adminPassword, self.config.getAdminUrl())
        return wls.mbs, wls.cmo

    def edit(self):
        print "Beginning edit session..."
        wls.edit()
        wls.startEdit()
        
    def activate(self):
        print "Activating changes..."
        wls.save()
        wls.activate(block="true")

    def disconnect(self): 
        print "Disconnecting from " + self.config.domain + " at " + self.config.getAdminUrl()
        
    def getTarget(self, name):
        targetMBean = wls.getMBean('/Servers/' + name)
        targetType = TargetType.SERVER
        if targetMBean is None:
            targetMBean = wls.getMBean('/Clusters/' + name)
            targetType = TargetType.CLUSTER
            if targetMBean is None:
                targetMBean = wls.getMBean('/MigratableTargets/' + name)
                targetType = TargetType.MIGRATABLE
        
        if targetMBean is None:
            return None, None
        else: 
            return (targetMBean, targetType)
    
    def getServersInCluster(self, clusterName):
        cluster = wls.getMBean('/Clusters/' + clusterName)
        
        if cluster is not None:
            servers = cluster.getServers()
            tmpList = [(x.getName(), x) for x in servers]
            tmpList.sort()
            serversList = [x for (key, x) in tmpList]
            return serversList
        else:
            return None
        
        
    def getFileStore(self, fileStore):
        return wls.getMBean('/FileStores/' + fileStore)
    
    def getJMSServer(self, jmsServer):
        return wls.getMBean('/JMSServers/' + jmsServer)
    
    def getJMSModule(self, jmsModule):
        return wls.getMBean('/JMSSystemResources/' + jmsModule)
    
    
    def getQueue(self, jmsModule, queue, distributed):
        if distributed:
            return wls.getMBean('/JMSSystemResources/' + jmsModule + '/JMSResource/' + jmsModule + '/UniformDistributedQueues/' + queue)
        else:
            return wls.getMBean('/JMSSystemResources/' + jmsModule + '/JMSResource/' + jmsModule + '/Queues/' + queue)
    
    def getSubDeployment(self, jmsModule, subDeployment):
        return wls.getMBean('/JMSSystemResources/' + jmsModule + '/SubDeployments/' + subDeployment)
    
    def createFileStore(self, storeName, target, path = ""):
        result = ExecutionResult()
        result.status = ExecutionStatus.SUCCESS
        
        targetMBean, targetType = self.getTarget(target)
        
        if targetMBean is None:
            result.status = ExecutionStatus.ERROR
            result.errorMessages.append("Invalid target " + target + " for File Store " + serverName)
            return result;
        
        fileStore = self.getFileStore(storeName)
        if fileStore is None:
            fileStore = wls.create(storeName, 'FileStore')
            fileStore.addTarget(targetMBean)
            fileStore.setDirectory(path)
            result.resultMBeans.append(fileStore)
            result.statusMessages.append('File Store ' + storeName + ' created')
            return result
        else:
            result.resultMBeans.append(fileStore)
            result.statusMessages.append('File Store ' + storeName + ' already exists')
            return result
    
    def getMBeanProperty(self, mbeanName, propertyName):
        mbean = wls.ObjectName(mbeanName)
        return wls.mbs.invoke(mbean, 'getProperty', [propertyName], ['java.lang.String'])

    def setMBeanProperty(self, mbeanName, propertyName, propertyValue):
        mbean = wls.ObjectName(mbeanName)
        return wls.mbs.invoke(mbean, 'setProperty', [propertyName, propertyValue], ['java.lang.String', 'java.lang.String'])
    
    def enableEMTargetCache(self, cacheAge, maxWaitTime):
            self.setMBeanProperty(self.EM_FMW_CTRL_MBEAN, self.FMW_DISCOVERY_USE_CACHED_RESULTS, 'true')
            self.setMBeanProperty(self.EM_FMW_CTRL_MBEAN, self.FMW_DISCOVERY_MAX_CACHE_AGE, str(cacheAge))
            self.setMBeanProperty(self.EM_FMW_CTRL_MBEAN, self.FMW_DISCOVERY_MAX_WAIT_TIME, str(maxWaitTime))        
        
    def disableEMTargetCache(self):
        self.setMBeanProperty(self.EM_FMW_CTRL_MBEAN, self.FMW_DISCOVERY_USE_CACHED_RESULTS, 'false')
        
    def printEMTargetCacheValues(self):
        print self.FMW_DISCOVERY_USE_CACHED_RESULTS + "=" + str(self.getMBeanProperty(self.EM_FMW_CTRL_MBEAN, self.FMW_DISCOVERY_USE_CACHED_RESULTS))
        print self.FMW_DISCOVERY_MAX_CACHE_AGE + "=" + str(self.getMBeanProperty(self.EM_FMW_CTRL_MBEAN, self.FMW_DISCOVERY_MAX_CACHE_AGE))
        print self.FMW_DISCOVERY_MAX_WAIT_TIME + "=" + str(self.getMBeanProperty(self.EM_FMW_CTRL_MBEAN, self.FMW_DISCOVERY_MAX_WAIT_TIME))
        
    def setFrontendHostAndPort(self, targetName, host, httpPort, httpsPort):
        result = ExecutionResult()
        result.status = ExecutionStatus.SUCCESS
        target, targetType = self.getTarget(targetName)
        if target != None:
            result.resultMBeans.append(target)
            
        httpMBean = None
        if targetType == TargetType.CLUSTER:
            httpMBean = target
            result.statusMessages.append('Cluster front end host and port configured')
        elif targetType == TargetType.SERVER:
            httpMBean = target.getWebServer()
            result.statusMessages.append('Server front end host and port configured')
        else:
            result.status = ExecutionStatus.ERROR
            result.errorMessages.append("Target was not a Server or Cluster")
            return result
        
        httpMBean.setFrontendHost(host)
        if httpPort != None and str(httpPort) != '':
            httpMBean.setFrontendHTTPPort(int(httpPort))
        if httpsPort != None and str(httpsPort) != '':
            httpMBean.setFrontendHTTPSPort(int(httpsPort))
        
            
        return result
    
    def setSoaUrls(self, soaTargets, host, httpPort, httpsPort):
        result = ExecutionResult()
        result.status = ExecutionStatus.SUCCESS
            
        url = ''
        for target in soaTargets:
            
            soaMBean = wls.ObjectName(self.SOA_CONFIG_MBEAN.replace('SERVER_NAME', target))
        
            if httpsPort != None and str(httpsPort) != '':
                url = 'https://' + host + ':' + httpsPor
                wls.mbs.setAttribute(soaMBean, Attribute('HttpServerURL', ''))
                wls.mbs.setAttribute(soaMBean, Attribute('HttpsServerURL', url))
                result.statusMessages.append("HttpsServerURL set to " + url)
            else:
                url = 'http://' + host + ':' + httpPort
                wls.mbs.setAttribute(soaMBean, Attribute('HttpServerURL', url))
                wls.mbs.setAttribute(soaMBean, Attribute('HttpsServerURL', ''))
                result.statusMessages.append("HttpServerURL set to " + url)
        
            wls.mbs.setAttribute(soaMBean, Attribute('CallbackServerURL', url))
            wls.mbs.setAttribute(soaMBean, Attribute('ServerURL', url))
            
        result.statusMessages.append("CallbackServerURL set to " + url)
        result.statusMessages.append("ServerURL set to " + url)
            
        return result
    
    def createGroup(self, groupName, groupDescription):
        provider = self.getDefaultAuthenticationProvider()
        if(not provider.groupExists(groupName)):
            provider.createGroup(groupName, groupDescription)
    
    def addMembersToGroup(self, groupName, members):
        provider = self.getDefaultAuthenticationProvider()
        for member in members:
            provider.addMemberToGroup(groupName, member)
        
        
    def getSecurityRealm(self):
        realm = wls.getMBean('/SecurityConfiguration/' + self.config.domain).getDefaultRealm()
        return realm
        
    def getDefaultAuthenticationProvider(self):
        realm = self.getSecurityRealm()
        defaultAuthenticationProvider = realm.lookupAuthenticationProvider('DefaultAuthenticator')
        return defaultAuthenticationProvider
    
    def createJdbcDatasource(self, dsConfig):
        result = ExecutionResult()
        result.status = ExecutionStatus.SUCCESS
        
        ds = wls.create(dsConfig.name, 'JDBCSystemResource')
        ds.setName(dsConfig.name)
        jdbcResource = ds.getJDBCResource()
        jdbcResource.setName(dsConfig.name)
        dsParams = jdbcResource.getJDBCDataSourceParams()
        driverParams = jdbcResource.getJDBCDriverParams()
        connectionParams = jdbcResource.getJDBCConnectionPoolParams()
        
        dsParams.addJNDIName(dsConfig.jndi)
        driverParams.setUrl(dsConfig.getUrl())
        driverParams.setDriverName(dsConfig.getDriver())
        driverParams.setPassword(dsConfig.password)
        connectionParams.setTestTableName(dsConfig.testTableName)
        
        driverProps = driverParams.getProperties()
        prop = driverProps.createProperty('user')
        prop.setValue(dsConfig.username)
        
        if(dsConfig.databaseType == DatabaseType.SQL_SERVER):
            prop = driverProps.createProperty('portNumber')
            prop.setValue(str(dsConfig.port))
            prop = driverProps.createProperty('databaseName')
            prop.setValue(dsConfig.databaseName)
            prop = driverProps.createProperty('serverName')
            prop.setValue(dsConfig.hostname)
        
        if(dsConfig.xaEnabled == False):
            dsParams.setGlobalTransactionsProtocol(str(dsConfig.transactionType))
        
        ds.setTargets(dsConfig.targets)
        
        result.resultMBeans.append(ds)
        result.statusMessages.append('JDBC Datasource ' + dsConfig.name + ' created')
        return result

    def createHumanWorkflowForeignJndi(self):
        result = ExecutionResult()
        result.status = ExecutionStatus.SUCCESS
        links = ['RuntimeConfigService', 
                 'TaskEvidenceServiceBean', 
                 'ejb/bpel/services/workflow/TaskMetadataServiceBean',
                 'ejb/bpel/services/workflow/TaskServiceBean',
                 'ejb/bpel/services/workflow/TaskServiceGlobalTransactionBean',
                 'ejb/bpm/services/BPMUserAuthenticationServiceBean',
                 'ejb/bpm/services/InstanceManagementServiceBean',
                 'ejb/bpm/services/InstanceQueryServiceBean',
                 'ejb/bpm/services/ProcessDashboardServiceBean',
                 'ejb/bpm/services/ProcessMetadataServiceBean',
                 'ejb/bpm/services/ProcessModelServiceBean',
                 'TaskQueryService',
                 'TaskReportServiceBean',
                 'UserMetadataService']
        jndiProvider = wls.getMBean('/Deployments/BPMRuntimeProvider')
        target, targetType = self.getTarget(self.config.getProperty('adf.targets'))
        if jndiProvider is None:
            jndiProvider = wls.create('BPMRuntimeProvider', 'ForeignJNDIProvider') 
            jndiProvider.setInitialContextFactory('weblogic.jndi.WLInitialContextFactory')
            jndiProvider.setProviderURL(self.config.getProperty('bpm.foreignJndi.url'))
            jndiProvider.setUser(self.config.getProperty('bpm.foreignJndi.user'))
            jndiProvider.setPassword(self.config.getProperty('bpm.foreignJndi.password'))
            jndiProvider.setTargets([target])
            result.statusMessages.append('BPMRuntimeProvider created')
        else:
            result.statusMessages.append('BPMRuntimeProvider already exists')
            
        for link in links:
            found = False
            for l in jndiProvider.foreignJNDILinks:
                if link == l.getName():
                    found = True
                    break
            if not found:
                newLink = jndiProvider.createForeignJNDILink(link)
                newLink.setLocalJNDIName(link)
                newLink.setRemoteJNDIName(link)
                result.statusMessages.append('Link ' + link + ' created')
            else:
                result.statusMessages.append('Link ' + link + ' already exists')
        
        return result
        

class JmsConfigurator(object):
    '''
    classdocs
    '''
    def __init__(self, weblogicConfig):
        self.weblogic = weblogicConfig
    
    def createModule(self, moduleName, moduleTargets):
        result = ExecutionResult()
        result.status = ExecutionStatus.SUCCESS
        
        # Check moduleTargets type to support a single target or a list of targets
        if(isinstance(moduleTargets, list)):
            targets = moduleTargets
        else:
            targets = [moduleTargets]
        
        jmsModule = wls.getMBean('/JMSSystemResources/' + moduleName)
        if jmsModule is None:
            try: 
                jmsModule = wls.create(moduleName, 'JMSSystemResource')
                result.statusMessages.append('JMS Module ' + moduleName + ' created.')
                result.resultMBeans.append(jmsModule)
            except Exception, e:
                result.status = ExecutionStatus.ERROR
                result.errorMessages.append('Error creating JMS Module ' + moduleName)
                result.exceptions.append(e)
                return result
                
            for target in targets:
                result = self._addTarget(jmsModule, moduleName, target, result)
                
        else:
            result.resultMBeans.append(jmsModule)
            result.statusMessages.append('JMS Module ' + moduleName + ' already exists.')
                
        return result
    
    def createServer(self, serverName, serverTarget, persistentStore):
        result = ExecutionResult()
        targetMBean, targetType = self.weblogic.getTarget(serverTarget)
        fileStore = None
        
        if persistentStore is not None:
            fileStore = self.weblogic.getFileStore(persistentStore)
            if fileStore is None:
                result.status = ExecutionStatus.ERROR
                result.errorMessages.append("Invalid persistent store " + persistentStore + " for JMS Server " + serverName)
                return result
        
        if targetMBean is None:
            result.status = ExecutionStatus.ERROR
            result.errorMessages.append("Invalid target " + serverTarget + " for JMS Server " + serverName)
            return result;
            
        jmsServer = wls.getMBean('/JMSServers/' + serverName)
        if jmsServer is None:
            try:
                jmsServer = wls.create(serverName, 'JMSServer')
                jmsServer.addTarget(targetMBean)
                if fileStore is not None:
                    jmsServer.setPersistentStore(fileStore)
                result.resultMBeans.append(jmsServer)
                result.statusMessages.append("JMS Server " + serverName + " created.")
            except Exception, e:
                result.status = ExecutionStatus.ERROR
                result.errorMessages.append("Error creating JMS Server " + serverName)
                return result
        else:
            result.resultMBeans.append(jmsServer)
            result.statusMessages.append("JMS Server " + serverName + " already exists")  
        return result
        
    def createSubdeployment(self, subDeploymentName, jmsModuleMBean, targets):    
        result = ExecutionResult()
        
        subDeployment = self.weblogic.getSubDeployment(jmsModuleMBean.getName(), subDeploymentName)
        if subDeployment is None:
            try:
                subDeployment = jmsModuleMBean.createSubDeployment(subDeploymentName)
                result.resultMBeans.append(subDeployment)
                result.status = ExecutionStatus.SUCCESS
                result.statusMessages.append("JMS Subdeployment " + subDeploymentName + " created.")
                
                for target in targets:
                        result = self._addJMSTarget(subDeployment, target, result)
                
                return result
            except Exception, e:
                result.status = ExecutionStatus.ERROR
                result.errorMessages.append("Error creating JMS Subdeployment " + subDeploymentName)
                return result  
        else:
            result.resultMBeans.append(subDeployment)
            result.status = ExecutionStatus.SUCCESS
            result.statusMessages.append("JMS Subdeployment " + subDeploymentName + " already exists")
            return result

    def _addJMSTarget(self, resourceMBean, target, result):
        targetMBean = self.weblogic.getJMSServer(target)
        if targetMBean is not None:
            resourceMBean.addTarget(targetMBean)
            result.statusMessages.append("Added JMS Server target " + target + " to " + resourceMBean.getName())
        else:
            msg = "Could not find target " + target + " to add to " + resourceMBean.getName()
            result.errorMessages.append(msg)
            result.status = ExecutionStatus.WARNING
            
        return result   
    
    def createQueue(self, jmsModuleMBean, queueName, queueJndi, queueSubdeployment, distributed):
        result = ExecutionResult()
        
        queue = self.weblogic.getQueue(jmsModuleMBean.getName(), queueName, distributed)
        if queue is None:
            try:
                if not distributed:
                    queue = jmsModuleMBean.getJMSResource().createQueue(queueName)
                    queue.setJNDIName(queueJndi)
                    queue.setSubDeploymentName(queueSubdeployment)
                    result.status = ExecutionStatus.SUCCESS
                    result.statusMessages.append("JMS Queue " + queueName + " created")
                    return result
                else:
                    queue = jmsModuleMBean.getJMSResource().createUniformDistributedQueue(queueName)
                    queue.setJNDIName(queueJndi)
                    queue.setSubDeploymentName(queueSubdeployment)
                    result.status = ExecutionStatus.SUCCESS
                    result.statusMessages.append("JMS Distributed Queue " + queueName + " created")
                    return result
            except Exception, e:
                result.status = ExecutionStatus.ERROR
                result.errorMessages.append("Error creating JMS Queue " + queueName)
                return result
        else:
            result.status = ExecutionStatus.SUCCESS
            result.statusMessages.append("JMS Queue " + queueName + " already exists")
            return result
        
        
    def _addTarget(self, resourceMBean, resourceName, target, result):
        targetMBean, targetType = self.weblogic.getTarget(target)
        if targetMBean is not None:
            resourceMBean.addTarget(targetMBean)
            if targetType == TargetType.SERVER:
                msg = "Added Server target " + target + " to " + resourceMBean.getName()
            elif targetType == TargetType.CLUSTER:
                msg = "Added Cluster target " + target + " to " + resourceMBean.getName()
            else:
                msg = "Added Migratable target " + target + " to " + resourceMBean.getName()
                
            result.statusMessages.append(msg)
        else:
            msg = "Could not find target " + target + " to add to " + resourceMBean.getName()
            result.errorMessages.append(msg)
            result.status = ExecutionStatus.WARNING
            
        return result     
        
class ExecutionResult(object):
    '''
    classdocs
    '''
    
    def __init__(self):
            self.status = ExecutionStatus.SUCCESS
            self.statusMessages = []
            self.errorMessages = []
            self.exceptions = []
            self.resultMBeans = []
    
    def __str__(self):
        result = "---------------------------------------------------------------------\n"
        result += "Execution Status: " + self.getStatus() + "\n"
        for s in self.statusMessages:
            result += "\t- " + s + "\n"
        for e in self.errorMessages: 
            result += "\t- " + e + "\n"
        result += "---------------------------------------------------------------------\n"
        for e in self.exceptions:
            result += str(type(e)) + "\n"
            result += str(e) + "\n"
        result += "---------------------------------------------------------------------\n"
        return result
        
    def getStatus(self):
        if(self.status == ExecutionStatus.SUCCESS):
            return "Success"
        elif(self.status == ExecutionStatus.ERROR):
            return "Error"
        else:
            return "Warning"
    
class ExecutionStatus(object):
    SUCCESS = 0
    WARNING = 1
    ERROR = -1

class TargetType(object):
    SERVER = 1
    CLUSTER = 2
    MIGRATABLE = 3   
    
class JdbcDatasource(object):
    SQL_SERVER_NON_XA_DRIVER = 'weblogic.jdbc.sqlserver.SQLServerDriver'
    ORACLE_XA_DRIVER = 'oracle.jdbc.xa.client.OracleXADataSource'
    ORACLE_DRIVER = 'oracle.jdbc.OracleDriver'
    
    def __init__(self):
        self.name = ''
        self.jndi = ''
        self.databaseType = -1
        self.xaEnabled = True
        self.transactionType = ''
        self.hostname = ''
        self.port = -1
        self.databaseName = ''
        self.username = ''
        self.password = ''
        self.targets = None
        self.testTableName = ''
        self.connectionType = DatabaseConnectionType.SERVICE
        self.properties = {}
    
    def setProperty(self, key, value):
        self.properties[key] = value
        
    def getProperty(self, key):
        return self.properties[key]
    
    def getDriver(self):
        if(self.databaseType == DatabaseType.SQL_SERVER and self.xaEnabled == False):
            return JdbcDatasource.SQL_SERVER_NON_XA_DRIVER
        elif(self.databaseType == DatabaseType.ORACLE and self.xaEnabled == True):
            return JdbcDatasource.ORACLE_XA_DRIVER
        
    def getUrl(self):
        if(self.databaseType == DatabaseType.SQL_SERVER):
            return "jdbc:weblogic:sqlserver://" + self.hostname + ":" + str(self.port)
        elif(self.databaseType == DatabaseType.ORACLE):
            url = 'jdbc:oracle:thin:@' + self.hostname + ':' + str(self.port)
            if(self.connectionType == DatabaseConnectionType.INSTANCE):
                return url + ':' + self.databaseName
            else:
                return url + '/' + self.databaseName  
    
class ClusterMessagingMode(object):
    UNICAST = 'unicast'
        
class TransactionType(object):
    EMULATE_TWO_PHASE_COMMIT = "EmulateTwoPhaseCommit"
    TWO_PHASE_COMMIT = 'TwoPhaseCommit'
    
class DatabaseType(object):
    ORACLE = 1
    SQL_SERVER = 2
    
class DatabaseConnectionType(object):
    INSTANCE = 'INSTANCE'
    SERVICE = 'SERVICE'
