import os
import sys
import fnmatch
import imp
from avio_fmw.fmw_environment import FmwEnvironnment



#=======================================================================================
#
# Usage: 
#      java weblogic.WLST <WLST_script> 
#
# Where: 
#      <WLST_script> specifies the full path to the WLST script.
#=======================================================================================

print """
###################################################
FMW Provisioning by AVIO Consulting
     __      _______ ____  
    /\ \    / /_   _/ __ \ 
   /  \ \  / /  | || |  | |
  / /\ \ \/ /   | || |  | |
 / ____ \  /   _| || |__| |
/_/    \_\/   |_____\____/
  _____                      _ _   _             
 / ____|                    | | | (_)            
| |     ___  _ __  ___ _   _| | |_ _ _ __   __ _ 
| |    / _ \| '_ \/ __| | | | | __| | '_ \ / _` |
| |___| (_) | | | \__ \ |_| | | |_| | | | | (_| |
 \_____\___/|_| |_|___/\__,_|_|\__|_|_| |_|\__, |
                                            __/ |
                                           |___/ 
###################################################
"""

def help():
    validEnvironments = getValidEnvironments()
    validCommands = getValidCommands()
    print "Usage: fmw-provisioning.py ENV COMMAND [ARGS]"
    print ""
    print "The following are valid environments for ENV:"
    print "---------------------------------------------"
    for e in validEnvironments:
        print e
    
    print ""
    print "The following are valid commands:"
    print "---------------------------------"
    for c in validCommands:
            print c
    print ""

def getValidCommands():
    return ['create-domain', 'configure-nm', 'rcu-create', 'rcu-drop']

def getValidEnvironments():
    validEnvironments = []
    
    f, pathname, description = imp.find_module('fmwconfig')
    if f:
        raise ImportError('Not a package: %r', 'fmwconfig')
    
    for module in os.listdir(pathname):
        if fnmatch.fnmatch(module, '*_config.py'):
            validEnvironments.append(str(module).replace('_config.py', '')) 
            
    return validEnvironments
    

def processArgs(args):    
    validEnvironments = getValidEnvironments()
    validCommands = getValidCommands()
    if len(args) < 2:
        print "Invalid Arguments: At least 2 arguments are required. See usage below."
        print ""
        return [None, None]
    if args[0] not in validEnvironments:
        print "Invalid Environment [" + args[0] + "]: See list of valid environments below."
        print ""
        return [None, None]
    if args[1] not in validCommands:
        print "Invalid Command [" + args[1] + "]: See list of valid commands below."
        print ""
        return [None, None]
        
    return (args[0], args[1])

def getWriteDomain():
    return True

def main(args):
    environment, command = processArgs(args)

    if environment == None or command == None:
        help()
    else:
        print 'Executing [' + command + '] for environment [' + environment + ']'
        
        env = FmwEnvironnment(environment=environment, writeDomain=getWriteDomain())
        env.loadConfiguration()
        
        if command == 'create-domain':
            env.createDomain()
        elif command == 'configure-nm':
            env.configureNodeManager()
        elif command == 'create-base-domain':
            env.createBaseDomain()
        elif command == 'rcu-create':
            env.rcuCreate()    
        elif command == 'rcu-drop':
            env.rcuDrop()                       
        else:
            print 'Invalid command: [' + command + ']'   
    
if __name__ == "__main__":
    main(sys.argv[1:])
        