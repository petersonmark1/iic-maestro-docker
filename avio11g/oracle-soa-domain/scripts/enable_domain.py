#!/usr/bin/python

import time
import getopt
import sys
import re
import os

# Get location of the properties file.
properties = ''
try:
   opts, args = getopt.getopt(sys.argv[1:],"p:h::",["properies="])
except getopt.GetoptError:
   print 'create_domain.py -p <path-to-properties-file>'
   sys.exit(2)
for opt, arg in opts:
   if opt == '-h':
      print 'create_domain.py -p <path-to-properties-file>'
      sys.exit()
   elif opt in ("-p", "--properties"):
      properties = arg
print 'properties=', properties

# Load the properties from the properties file.
from java.io import FileInputStream
 
propInputStream = FileInputStream(properties)
configProps = Properties()
configProps.load(propInputStream)

# Set all variables from values in properties file.
middlewarePath=configProps.get("path.middleware")
domainConfigPath=configProps.get("path.domain.config")
domainName=configProps.get("domain.name")
dbUrl=configProps.get("db.dbUrl")
mdsUser=configProps.get("db.mdsUser")
mdsPassword=configProps.get("db.mdsPassword")
mdsDatasource=configProps.get("db.mdsDatasource")
XADatasourceDriver=configProps.get("db.XADatasourceDriver")

# Display the variable values.
print 'middlewarePath=', middlewarePath
print 'domainConfigPath=', domainConfigPath
print 'domainName=', domainName
print 'dbUrl=', dbUrl
print 'mdsPassword=', mdsPassword
print 'mdsUser=', mdsUser
print 'mdsDatasource=', mdsDatasource
print 'DatasourceDriver=', XADatasourceDriver
print 'jrf_template=', middlewarePath,'/oracle_common/common/templates/applications/jrf_template_11.1.1.jar'

readDomain(domainConfigPath + '/' + domainName)

print 'addTemplate=', 'Oracle JRF Asynchronous Web Services Template =', middlewarePath, '/oracle_common/common/templates/applications/jrf_template_11.1.1.jar'
addTemplate(middlewarePath + '/oracle_common/common/templates/applications/jrf_template_11.1.1.jar')

print 'addTemplate=', 'Enterprise Manager Template =', middlewarePath, '/oracle_common/common/templates/applications/em_11_1_1_0_0_template.jar'
addTemplate(middlewarePath + '/oracle_common/common/templates/applications/oracle.em_11_1_1_0_0_template.jar')

print 'addTemplate=', 'Oracle Web Service Manager (OWSM) Template =', middlewarePath, '/oracle_common/common/templates/applications/oracle.wsmpm_template_11.1.1.jar'
addTemplate(middlewarePath + '/oracle_common/common/templates/applications/oracle.wsmpm_template_11.1.1.jar')

#cd('/JDBCSystemResources/' + mdsDatasource + '/JDBCResource/' + mdsDatasource  + '/JDBCDriverParams/' + mdsDatasource)
#cmo.setUrl(mdsUrl)
#set('Password', mdsPassword)

#cd('/JDBCSystemResources/' + mdsDatasource + '/JDBCResource/' + mdsDatasource + '/JDBCDriverParams/' + mdsDatasource + '/Properties/' + mdsDatasource + '/Properties/user')

#cmo.setValue(mdsUser)

print 'updateDomain'
updateDomain()
closeDomain()
exit()
