# Set environment.
export MW_HOME=/opt/oracle/product/fmw
export WLS_HOME=$MW_HOME/wlserver_10.3
export WL_HOME=$WLS_HOME
export JAVA_HOME=/opt/oracle/product/java
export PATH=$JAVA_HOME/bin:$PATH
export CONFIG_JVM_ARGS=-Djava.security.egd=file:/dev/./urandom

. $WLS_HOME/server/bin/setWLSEnv.sh

# Create the domain.
echo "***********************"
echo "CREATING DOMAIN"
echo "***********************"
java weblogic.WLST /home/oracle/scripts/create_domain.py -p /home/oracle/scripts/myDomain.properties

# Enable the domain.
echo "***********************"
echo "ENABLING DOMAIN"
echo "***********************"
java weblogic.WLST /home/oracle/scripts/enable_domain.py -p /home/oracle/scripts/myDomain.properties
