'''
Created on Jul 20, 2015

@author: adesjard
'''
from avio_fmw.weblogic_config import WeblogicConfig
from avio_fmw.weblogic_configurator import WeblogicOfflineConfigurator, ClusterMessagingMode
import os

class FmwEnvironnment(object):
    
    SOA_TEMPLATE = '/soa/common/templates/wls/oracle.soa_template.jar'
    BPM_TEMPLATE = '/soa/common/templates/wls/oracle.bpm_template.jar'
    OSB_TEMPLATE = '/osb/common/templates/wls/oracle.osb_template.jar'
    BAM_TEMPLATE = '/soa/common/templates/wls/oracle.bam.server_template.jar'
    WSM_TEMPLATE = '/oracle_common/common/templates/wls/oracle.wsmpm_template.jar'

    def __init__(self, environment, writeDomain = True):
        self.environment = environment
        self.writeDomain = writeDomain
        self.domainLoaded = False
        
    def loadConfiguration(self):
        print 'Loading configuration for ' + self.environment
        self.config = __import__('fmwconfig.' + self.environment + "_config").__getattribute__(self.environment + '_config')
        self.weblogicConfig = WeblogicConfig(self.config.ADMIN_HOST, 
                                             self.config.ADMIN_PORT, 
                                             self.config.ADMIN_USER,
                                             self.config.ADMIN_PASSWORD,
                                             self.config.DOMAIN,
                                             self.config.SOA_CLUSTER_NAME)
        self.offlineConfig = WeblogicOfflineConfigurator(self.weblogicConfig)
    
    def loadDomain(self):
        if not self.domainLoaded:
            self.offlineConfig.readDomain(self.config.DOMAIN_HOME, self.config.APP_HOME)  
    
    def configureNodeManager(self):
        self.offlineConfig.createStartupProps(self.config.DOMAIN_HOME, self.config.ADMIN_SERVER, self.config.ADMIN_START_ARGS)
        self.offlineConfig.createNmPasswordProps(self.config.DOMAIN_HOME, self.config.ADMIN_USER, self.config.ADMIN_PASSWORD)
        self.offlineConfig.configureNodeManager(self.config.DOMAIN_HOME, self.config.ADMIN_MACHINE_HOST, self.config.ADMIN_MACHINE_PORT, self.config.ADMIN_MACHINE_TYPE)      
        
        
    def createDomain(self):
        print 'Creating domain...'
        self.createBaseDomain()
        
        self.loadDomain() 
        # Create machine definitions for managed nodes if at least one product is enabled
        if self.config.ENABLE_SOA or self.config.ENABLE_BPM or self.config.ENABLE_OSB or self.config.ENABLE_BAM or self.config.ENABLE_WSM:    
            self.createMachines()

            if self.config.ENABLE_WSM:
                self.addWSMToDomain()
            
            if self.config.ENABLE_SOA or self.config.ENABLE_BPM:
                self.addSOAToDomain()
            
            if self.config.ENABLE_OSB:
                self.addOSBToDomain()
            
            if self.config.ENABLE_BAM:
                self.addBAMToDomain()
            
            self.offlineConfig.setServerGroups(self.config.ADMIN_SERVER, self.config.ADMIN_SERVER_GROUPS)
            
            if self.config.ENABLE_BPM:
                self.targetBPMOsgi()

	    if self.config.ENABLE_WSM:
		self.offlineConfig.setApplicationTargets('wsm-pm', self.config.WSM_CLUSTER_NAME)
            
        self.offlineConfig.updateDomain()
        
        # Configure node manager property files last so they are not over written
        self.configureNodeManager()
    
    def createBaseDomain(self):
        self.offlineConfig.createDomain(self.config.BASE_TEMPLATE, 
                                    self.config.JAVA_HOME, 
                                    self.config.APP_BASE, 
                                    self.config.DEVELOPMENT_MODE, 
                                    self.config.ADMIN_USER, 
                                    self.config.ADMIN_PASSWORD)
        
        self.offlineConfig.configureDomainLogs(self.config.DOMAIN, 
                                           self.config.LOG_COUNT, 
                                           self.config.LOG_SIZE, 
                                           self.config.LOG_DIR)
        
        self.offlineConfig.createMachine(self.config.ADMIN_MACHINE, 
                                         self.config.ADMIN_MACHINE_HOST,
                                         self.config.ADMIN_MACHINE_PORT,
                                         self.config.ADMIN_MACHINE_TYPE)
        
        self.offlineConfig.configureServer(self.config.ADMIN_SERVER, 
                                           self.config.ADMIN_MACHINE, 
                                           self.config.ADMIN_HOST, 
                                           self.config.ADMIN_PORT, 
                                           self.config.ADMIN_START_ARGS, 
                                           self.config.JAVA_HOME, 
                                           self.config.JSSE_ENABLED)
        
        self.offlineConfig.configureServerLogs(self.config.ADMIN_SERVER, 
                                              self.config.LOG_COUNT, 
                                              self.config.LOG_SIZE, 
                                              self.config.LOG_DIR)
        
        if self.writeDomain:
            self.offlineConfig.writeDomain(self.config.DOMAIN_HOME)
            
        self.offlineConfig.readDomain(self.config.DOMAIN_HOME, self.config.APP_HOME)    
        self.offlineConfig.setDomainCredential(self.config.DOMAIN, self.config.ADMIN_PASSWORD, self.config.DOMAIN_HOME)
        self.offlineConfig.setNmCredential(self.config.DOMAIN, self.config.DOMAIN_HOME, self.config.ADMIN_USER, self.config.ADMIN_PASSWORD)
        self.offlineConfig.createBootProps(self.config.DOMAIN_HOME, self.config.ADMIN_SERVER, self.config.ADMIN_USER, self.config.ADMIN_PASSWORD)
        self.offlineConfig.createUserOverrides(self.config.DOMAIN_HOME)
        self.offlineConfig.updateDomain()
                
    
    def addSOAToDomain(self):
            self.offlineConfig.addTemplate(self.config.ORACLE_HOME + self.SOA_TEMPLATE)
            
            if self.config.ENABLE_BPM:
                self.offlineConfig.addTemplate(self.config.ORACLE_HOME + self.BPM_TEMPLATE)
                
            self.offlineConfig.configureServiceTableConnection(self.config.DB_URL, self.config.DB_PREFIX, self.config.DB_PASSWORD)
            self.offlineConfig.getDatabaseDefaults()
            self.offlineConfig.configureDataSourceForXA('SOADataSource')
            self.offlineConfig.configureDataSourceForXA('EDNDataSource')
            self.offlineConfig.configureDataSourceForXA('OraSDPMDataSource')
            
            if self.config.DB_GRIDLINK:
                self.offlineConfig.configureDataSourceForGridlink('opss-data-source')
                self.offlineConfig.configureDataSourceForGridlink('opss-audit-viewDS')
                self.offlineConfig.configureDataSourceForGridlink('opss-audit-DBDS')
                self.offlineConfig.configureDataSourceForGridlink('mds-owsm')
                self.offlineConfig.configureDataSourceForGridlink('OraSDPMDataSource')
                self.offlineConfig.configureDataSourceForGridlink('EDNDataSource')
                self.offlineConfig.configureDataSourceForGridlink('EDNLocalTxDataSource')
                self.offlineConfig.configureDataSourceForGridlink('mds-soa')
                self.offlineConfig.configureDataSourceForGridlink('SOADataSource')
                self.offlineConfig.configureDataSourceForGridlink('SOALocalTxDataSource')
            
            self.offlineConfig.createCluster(self.config.SOA_CLUSTER_NAME, ClusterMessagingMode.UNICAST)
            self.offlineConfig.configureClusterHttp(self.config.SOA_CLUSTER_NAME, self.config.SOA_HTTP_PLUGIN_ENABLED, self.config.SOA_FRONTEND_HOST, self.config.SOA_FRONTEND_HTTP_PORT, self.config.SOA_FRONTEND_HTTPS_PORT)
            
            for x in range(1, self.config.MANAGED_NODES + 1):
                serverName = self.config.SOA_SERVER_BASE_NAME + str(x)
                if(x == 1):
                    self.offlineConfig.renameServer('soa_server1', serverName)
                else:
                    self.offlineConfig.createServer(serverName)
                
                self.offlineConfig.configureServer(serverName, 
                                                   'SOA_MACHINE' + str(x), 
                                                   self.config.SOA_HOSTS[x-1], 
                                                   self.config.SOA_PORTS[x-1], 
                                                   self.config.SOA_START_ARGS, 
                                                   self.config.JAVA_HOME, 
                                                   self.config.JSSE_ENABLED)
                self.offlineConfig.configureServerLogs(serverName, 
                                                  self.config.LOG_COUNT, 
                                                  self.config.LOG_SIZE, 
                                                  self.config.LOG_DIR)
                self.offlineConfig.configureTLogs(serverName, self.config.SOA_TLOG_DIR)

                self.offlineConfig.addServerToCluster(serverName, self.config.SOA_CLUSTER_NAME)
                self.offlineConfig.setServerGroups(serverName, self.config.SOA_SERVER_GROUPS)

    def addOSBToDomain(self):
            self.offlineConfig.addTemplate(self.config.ORACLE_HOME + self.OSB_TEMPLATE)
        
                
            self.offlineConfig.configureServiceTableConnection(self.config.DB_URL, self.config.DB_PREFIX, self.config.DB_PASSWORD)
            self.offlineConfig.getDatabaseDefaults()
            self.offlineConfig.configureDataSourceForXA('wlsbjmsrpDataSource')

            if self.config.DB_GRIDLINK:
                self.offlineConfig.configureDataSourceForGridlink('opss-data-source')
                self.offlineConfig.configureDataSourceForGridlink('opss-audit-viewDS')
                self.offlineConfig.configureDataSourceForGridlink('opss-audit-DBDS')
                self.offlineConfig.configureDataSourceForGridlink('mds-owsm')
                self.offlineConfig.configureDataSourceForGridlink('wlsbjmsrpDataSource')

            
            self.offlineConfig.createCluster(self.config.OSB_CLUSTER_NAME, ClusterMessagingMode.UNICAST)
            self.offlineConfig.configureClusterHttp(self.config.OSB_CLUSTER_NAME, self.config.OSB_HTTP_PLUGIN_ENABLED, self.config.OSB_FRONTEND_HOST, self.config.OSB_FRONTEND_HTTP_PORT, self.config.OSB_FRONTEND_HTTPS_PORT)
            
            for x in range(1, self.config.MANAGED_NODES + 1):
                serverName = self.config.OSB_SERVER_BASE_NAME + str(x)
                if(x == 1):
                    self.offlineConfig.renameServer('osb_server1', serverName)
                else:
                    self.offlineConfig.createServer(serverName)
                
                self.offlineConfig.configureServer(serverName, 
                                                   'SOA_MACHINE' + str(x), 
                                                   self.config.OSB_HOSTS[x-1], 
                                                   self.config.OSB_PORTS[x-1], 
                                                   self.config.OSB_START_ARGS, 
                                                   self.config.JAVA_HOME, 
                                                   self.config.JSSE_ENABLED)
                self.offlineConfig.configureServerLogs(serverName, 
                                                  self.config.LOG_COUNT, 
                                                  self.config.LOG_SIZE, 
                                                  self.config.LOG_DIR)
                self.offlineConfig.configureTLogs(serverName, self.config.OSB_TLOG_DIR)

                self.offlineConfig.addServerToCluster(serverName, self.config.OSB_CLUSTER_NAME)
                self.offlineConfig.setServerGroups(serverName, self.config.OSB_SERVER_GROUPS)  
                
    def addBAMToDomain(self):
            self.offlineConfig.addTemplate(self.config.ORACLE_HOME + self.BAM_TEMPLATE)
        
                
            self.offlineConfig.configureServiceTableConnection(self.config.DB_URL, self.config.DB_PREFIX, self.config.DB_PASSWORD)
            self.offlineConfig.getDatabaseDefaults()
            self.offlineConfig.configureDataSourceForXA('BamDataSource')

            if self.config.DB_GRIDLINK:
                self.offlineConfig.configureDataSourceForGridlink('opss-data-source')
                self.offlineConfig.configureDataSourceForGridlink('opss-audit-viewDS')
                self.offlineConfig.configureDataSourceForGridlink('opss-audit-DBDS')
                self.offlineConfig.configureDataSourceForGridlink('mds-owsm')
                self.offlineConfig.configureDataSourceForGridlink('OraSDPMDataSource')
                self.offlineConfig.configureDataSourceForGridlink('mds-bam')
                self.offlineConfig.configureDataSourceForGridlink('BamDataSource')
                self.offlineConfig.configureDataSourceForGridlink('BamJobSchedDataSource')

            
            self.offlineConfig.createCluster(self.config.BAM_CLUSTER_NAME, ClusterMessagingMode.UNICAST)
            self.offlineConfig.configureClusterHttp(self.config.BAM_CLUSTER_NAME, self.config.BAM_HTTP_PLUGIN_ENABLED, self.config.BAM_FRONTEND_HOST, self.config.BAM_FRONTEND_HTTP_PORT, self.config.BAM_FRONTEND_HTTPS_PORT)
            
            for x in range(1, self.config.MANAGED_NODES + 1):
                serverName = self.config.BAM_SERVER_BASE_NAME + str(x)
                if(x == 1):
                    self.offlineConfig.renameServer('bam_server1', serverName)
                else:
                    self.offlineConfig.createServer(serverName)
                
                self.offlineConfig.configureServer(serverName, 
                                                   'SOA_MACHINE' + str(x), 
                                                   self.config.BAM_HOSTS[x-1], 
                                                   self.config.BAM_PORTS[x-1], 
                                                   self.config.BAM_START_ARGS, 
                                                   self.config.JAVA_HOME, 
                                                   self.config.JSSE_ENABLED)
                self.offlineConfig.configureServerLogs(serverName, 
                                                  self.config.LOG_COUNT, 
                                                  self.config.LOG_SIZE, 
                                                  self.config.LOG_DIR)
                self.offlineConfig.configureTLogs(serverName, self.config.BAM_TLOG_DIR)

                self.offlineConfig.addServerToCluster(serverName, self.config.BAM_CLUSTER_NAME)
                self.offlineConfig.setServerGroups(serverName, self.config.BAM_SERVER_GROUPS)                           

    def addWSMToDomain(self):
            self.offlineConfig.addTemplate(self.config.ORACLE_HOME + self.WSM_TEMPLATE)


            self.offlineConfig.configureServiceTableConnection(self.config.DB_URL, self.config.DB_PREFIX, self.config.DB_PASSWORD)
            self.offlineConfig.getDatabaseDefaults()

            if self.config.DB_GRIDLINK:
                self.offlineConfig.configureDataSourceForGridlink('opss-data-source')
                self.offlineConfig.configureDataSourceForGridlink('opss-audit-viewDS')
                self.offlineConfig.configureDataSourceForGridlink('opss-audit-DBDS')
                self.offlineConfig.configureDataSourceForGridlink('mds-owsm')


            self.offlineConfig.createCluster(self.config.WSM_CLUSTER_NAME, ClusterMessagingMode.UNICAST)
            self.offlineConfig.configureClusterHttp(self.config.WSM_CLUSTER_NAME, self.config.WSM_HTTP_PLUGIN_ENABLED, self.config.WSM_FRONTEND_HOST, self.config.WSM_FRONTEND_HTTP_PORT, self.config.WSM_FRONTEND_HTTPS_PORT)

            for x in range(1, self.config.MANAGED_NODES + 1):
                serverName = self.config.WSM_SERVER_BASE_NAME + str(x)
                self.offlineConfig.createServer(serverName)

                self.offlineConfig.configureServer(serverName,
                                                   'SOA_MACHINE' + str(x),
                                                   self.config.WSM_HOSTS[x-1],
                                                   self.config.WSM_PORTS[x-1],
                                                   self.config.WSM_START_ARGS,
                                                   self.config.JAVA_HOME,
                                                   self.config.JSSE_ENABLED)
                self.offlineConfig.configureServerLogs(serverName,
                                                  self.config.LOG_COUNT,
                                                  self.config.LOG_SIZE,
                                                  self.config.LOG_DIR)
                self.offlineConfig.configureTLogs(serverName, self.config.WSM_TLOG_DIR)

                self.offlineConfig.addServerToCluster(serverName, self.config.WSM_CLUSTER_NAME)
                self.offlineConfig.setServerGroups(serverName, self.config.WSM_SERVER_GROUPS)
    
    def createMachines(self):
        """
        Creates X number of SOA_MACHINEs based on the number of configured nodes
        """    
        for x in range(1, self.config.MANAGED_NODES + 1):
            machineName = 'SOA_MACHINE' + str(x)
            print('Creating machine [' + machineName + ']')
            self.offlineConfig.createMachine(machineName, self.config.MANAGED_NM_HOSTS[x-1], self.config.MANAGED_NM_PORTS[x-1], self.config.MANAGED_NM_TYPE)
            
    def targetBPMOsgi(self):
        """
        Re-targets the OSGI container used for BPM because other templates remove the targeting from the SOA Cluster
        """
        print('Re-targeting BPM OSGI Container')
        self.offlineConfig.setAttributes('/OsgiFramework/bac-svnserver-osgi-framework', {'Target' : self.config.SOA_CLUSTER_NAME + ',AdminServer'})
        
    def configureDomain(self):
        print 'Configuring domain...'
        
    def rcuCreate(self):
        self.executeRcuCommand('createRepository')
        
    def rcuDrop(self):
        self.executeRcuCommand('dropRepository')
     
    def executeRcuCommand(self, command):
        cmd = self.config.ORACLE_HOME + '/oracle_common/bin/rcu -silent -' + command + ' -databaseType ORACLE'
        cmd += ' -connectString ' + self.config.DB_HOST + ':' + self.config.DB_PORT + ':' + self.config.DB_SERVICE
        cmd += ' -dbUser sys -dbRole sysdba '
        if (command == 'createRepository'):
            cmd += ' -useSamePasswordForAllSchemaUsers true '		
        cmd += ' -schemaPrefix ' + self.config.DB_PREFIX
        
        if os.path.isfile('/tmp/rcupasswd'):
            os.remove('/tmp/rcupasswd')
        f = open('/tmp/rcupasswd', 'w')
        f.write(self.config.DB_SYS_PASSWORD + '\n')
        f.write(self.config.DB_PASSWORD + '\n')
        f.close()       
        
        for s in self.config.DB_SCHEMAS:
            cmd += ' -component ' + s
            
        cmd += ' -f < /tmp/rcupasswd'
        
        print 'Executing RCU command: ' + cmd
        os.system(cmd)
